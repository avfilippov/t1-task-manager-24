package ru.t1.avfilippov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @NotNull
    String SECRET = "123123";

    @NotNull
    Integer ITERATION = 755;

    static String salt(@Nullable final String value) {
        if (value== null) return null;
        @Nullable String result = value;
        for (int i = 0; i< ITERATION; i++) {
            result = md5(SECRET+result+SECRET);
        }
        return result;
    }

    static String md5(@NotNull final String value) {
        try {
            @NotNull final MessageDigest md = MessageDigest.getInstance("MD5");
            @NotNull final byte[] bytes = md.digest(value.getBytes());
            @NotNull final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (@NotNull NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
