package ru.t1.avfilippov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.model.User;

public interface IUserRepository extends IRepository<User>{

    @NotNull
    User create (@NotNull String login, @NotNull String password);

    @NotNull
    User create (@NotNull String login, @NotNull String password, @Nullable String email);

    @NotNull
    User create (@NotNull String login, @NotNull String password, @Nullable Role role);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User remove(@Nullable User user);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

}
