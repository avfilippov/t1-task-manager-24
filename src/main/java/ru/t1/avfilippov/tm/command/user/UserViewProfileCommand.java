package ru.t1.avfilippov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "view-user-profile";

    @NotNull
    private final String DESCRIPTION = "view profile of current user";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[VIEW USER PROFILE]");
        @Nullable final User user = serviceLocator.getAuthService().getUser();
        System.out.println("ID: "+user.getId());
        System.out.println("LOGIN: "+user.getLogin());
        System.out.println("FIRST NAME: "+ user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: "+ user.getLastName());
        System.out.println("EMAIL: "+ user.getEmail());
        System.out.println("ROLE: "+ user.getRole().getDisplayName());
    }

}
