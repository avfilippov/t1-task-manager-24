package ru.t1.avfilippov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.util.FormatUtil;

public final class SystemInfoCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getArgument() {
        return "-i";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "show system info";
    }

    @NotNull
    @Override
    public String getName() {
        return "info";
    }

    @NotNull
    @Override
    public void execute() {
        @NotNull final Runtime runtime = Runtime.getRuntime();
        final long availableProcessors = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        @NotNull final String freeMemoryFormat = FormatUtil.format(freeMemory);
        final long maxMemory = runtime.maxMemory();
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        @NotNull final String maxMemoryFormat = (maxMemoryCheck ? "no limit" : FormatUtil.format(maxMemory));
        final long totalMemory = runtime.totalMemory();
        @NotNull final String totalMemoryFormat = FormatUtil.format(totalMemory);
        final long usageMemory = totalMemory - freeMemory;
        @NotNull final String usageMemoryFormat = FormatUtil.format(usageMemory);

        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Maximum memory: " + maxMemoryFormat);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Usage memory: " + usageMemoryFormat);
    }

}
