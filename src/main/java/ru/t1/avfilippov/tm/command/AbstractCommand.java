package ru.t1.avfilippov.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.model.ICommand;
import ru.t1.avfilippov.tm.api.service.IAuthService;
import ru.t1.avfilippov.tm.api.service.IServiceLocator;
import ru.t1.avfilippov.tm.enumerated.Role;

@Getter
@Setter
public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    public IAuthService getAuthService () {
        return serviceLocator.getAuthService();
    }

    @Nullable
    public String getUserId() {
        return getAuthService().getUserId();
    }

    @NotNull
    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += ": " + description;
        return result;
    }

}
