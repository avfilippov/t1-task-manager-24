package ru.t1.avfilippov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getArgument() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "show application version";
    }

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.24.0");
    }

}
