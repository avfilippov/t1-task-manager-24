package ru.t1.avfilippov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.model.Project;
import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "remove project by index";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final String userId = getUserId();
        @Nullable final Project project = getProjectService().findOneByIndex(userId, index);
        if (project != null) getProjectTaskService().removeProjectById(userId, project.getId());
    }

}
