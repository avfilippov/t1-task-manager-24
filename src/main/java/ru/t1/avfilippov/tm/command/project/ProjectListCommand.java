package ru.t1.avfilippov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.enumerated.Sort;
import ru.t1.avfilippov.tm.model.Project;
import ru.t1.avfilippov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "display all projects";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @Nullable final String userId = getUserId();
        @NotNull final List<Project> projects = getProjectService().findAll(userId, sort);
        int index = 1;
        for (@NotNull final Project project : projects) {
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

}
