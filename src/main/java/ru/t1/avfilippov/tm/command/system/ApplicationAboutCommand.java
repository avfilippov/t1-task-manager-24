package ru.t1.avfilippov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "show developer's info";
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Alexey Filippov");
        System.out.println("Email: avfilippov@t1-consulting.ru");
    }

}
